import sys
from datetime import datetime, timedelta, timezone, date
from datetime import timedelta
from apmondatalib import DataFetcher, AIslandClient, Constants
import pandas as pd
import flask
from flask import request
from flask import Response
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
import warnings
import werkzeug
import optparse
import tornado.wsgi
import tornado.httpserver
import time
import logging
import json
from pmdarima.arima import auto_arima
from fbprophet import Prophet

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

app = flask.Flask(__name__)

#sensor_types_dict = {Constants.SensorType.Temperature : 'temperature'}

sensor_types_dict = {Constants.SensorType.Humidity : 'humidity',
                     Constants.SensorType.Temperature : 'temperature',
                     Constants.SensorType.Light : 'light'}
sensor_types = [DataFetcher.SensorType.Humidity, DataFetcher.SensorType.Temperature, DataFetcher.SensorType.Light]
#sensor_types = [Constants.SensorType.Temperature]
'''
sensor_types = [Constants.SensorType.Humidity,
                Constants.SensorType.Temperature,
                Constants.SensorType.Light]
'''
def http_error_response(error_msg, status_code):
    data = {
        'error_message': error_msg
    }

    js = json.dumps(data)

    res = Response(js, status=status_code, mimetype='application/json')
    logger.error(error_msg)
    return res

def http_success_response():
    data = {
        'status_message': 'success'
    }

    js = json.dumps(data)

    res = Response(js, status=200, mimetype='application/json')
    return res


def validate_date_string(date_string):
    result = True

    try:
        datetime.strptime(date_string, '%Y-%m-%d')
    except ValueError:
        result = False

    return result

def evaluate_arima_model(X, arima_order):

    train_size = int(len(X) * 0.66)
    train, test = X[0:train_size], X[train_size:]
    history = [x for x in train]

    predictions = list()
    for t in range(len(test)):
        model = ARIMA(history, order=arima_order)
        model_fit = model.fit(trend='nc', disp=0)
        yhat = model_fit.forecast()[0]
        predictions.append(yhat)
        history.append(test[t])

    error = mean_squared_error(test, predictions)
    return error

def evaluate_models(dataset, p_values, d_values, q_values):
    dataset = dataset.astype('float32')
    best_score, best_cfg = float("inf"), None
    for p in p_values:
        for d in d_values:
            for q in q_values:
                order = (p,d,q)
                try:
                    mse = evaluate_arima_model(dataset, order)
                    if mse < best_score:
                        best_score, best_cfg = mse, order
                    logger.info('ARIMA%s MSE=%.3f' % (order,mse))
                except:
                    continue

    logger.info('Best ARIMA%s MSE=%.3f' % (best_cfg, best_score))

    return best_cfg

def insert_daily_summary_into_mongodb(df, client, data_fetcher, sensor_type, from_date, to_date, predict_unit, predict_steps, sensor_id):
    #p_values = [0, 1, 2, 4, 6, 8, 10]
    p_values = [0, 1, 2, 4, 6]
    d_values = range(0, 2)
    q_values = range(0, 3)
   
    prediction_date = to_date

    logger.info("== %s, sensor_type: %d", sensor_id, sensor_type)
    logger.info("== %s, sensor_types_dict[sensor_type]: %s", sensor_id, sensor_types_dict[sensor_type])
    logger.info("== %s, predict_unit: %s", sensor_id, predict_unit)

    now_time = time.time()
    s = data_fetcher.read_in_range_with_date(sensor_type, from_date, to_date)
    logger.info("== data_fetcher.read_in_range_with_date time elapsed: %d", time.time()-now_time)

    if predict_unit == "daily":
        for x in s:
            df = df.append({'ds': datetime(x.year, x.month, x.day, 0, 0, 0, 0), 'y': x.average}, ignore_index=True)

        df = df.dropna()

        if df['y'].size == 0:
            return http_error_response('There is no data results => ' + "sendsor_id: " + sensor_id + ", sensor type: " + sensor_types_dict[sensor_type], 404)
        else:
            logger.info('== data is ok')

            try:
                #best_pdq = evaluate_models(df[sensor_types_dict[sensor_type]].values, p_values, d_values, q_values)

                #model = ARIMA(df[sensor_types_dict[sensor_type]].values, order=best_pdq)
                #model_fit = model.fit(disp=0)

                #forecast_result = model_fit.forecast(steps=predict_steps)
                #forecast_list = forecast_result[0].tolist()
                m = Prophet(changepoint_prior_scale=0.5)
                m.fit(df)
                future = m.make_future_dataframe(periods=predict_steps)
                forecast = m.predict(future)
                forecast_list = forecast['yhat'][:predict_steps]
  
                logger.debug("== forecast_list:%s", forecast_list)

                for step in range(0, predict_steps):
                    prediction_date = prediction_date + timedelta(days=1)

                    body = {"0": {"average": forecast_list[step]}}
                    ret = client.get_sensor(sensor_id).get_daily_summary_fetcher().append_prediction(sensor_type,
                                                                                                     prediction_date.year,
                                                                                                     prediction_date.month,
                                                                                                     prediction_date.day,
                                                                                                     body)
                    logger.debug("== sensor type: %s, prediction_date: %s, append_prediction: %s, body: %s", sensor_type, prediction_date, ret, body)
            except BaseException as e:
                return http_error_response("ARIMA Error: " + str(e) + " => sendsor_id: " + sensor_id + ", sensor type: " + sensor_types_dict[sensor_type], 404)

    elif predict_unit == "hourly":
        for x in s:
            for m in x.data_set:
                #print(m)
                df = df.append({'ds': datetime(x.year, x.month, x.day, m.hour, 0, 0, 0), 'y': m.average}, ignore_index=True)

        df = df.dropna()
        #print(df)


        if df['y'].size == 0:
            return http_error_response('There is no data results => ' + "sendsor_id: " + sensor_id + ", sensor type: " + sensor_types_dict[sensor_type], 404)
        else:
            logger.info('== data is ok')

            try:
                m = Prophet(changepoint_prior_scale=0.5)
                m.fit(df)
                future = m.make_future_dataframe(periods=predict_steps)
                forecast = m.predict(future)
                forecast_list = forecast['yhat'][:predict_steps]
                '''
                stepwise_model = auto_arima(df[sensor_types_dict[sensor_type]].values, start_p=0, start_q=0,start_d=0, max_d=2,
                           max_p=10, max_q=5, m=1,max_order=15,
                           start_P=0, seasonal=False,stationary=True,maxiter=10000,
                           d=1, D=1, trace=True,
                           error_action='ignore',  
                           suppress_warnings=True, 
                           stepwise=True)
                
                
                stepwise_model.fit(df[sensor_types_dict[sensor_type]].values)
                forecast_list = stepwise_model.predict(n_periods=predict_steps)
                '''
                '''
                best_pdq = evaluate_models(df[sensor_types_dict[sensor_type]].values, p_values, d_values, q_values)
                #best_pdq = (2,0,1)

                model = ARIMA(df[str(sensor_types_dict[sensor_type])].values, order=best_pdq)
                model_fit = model.fit(disp=0)
                forecast_result = model_fit.forecast(steps=predict_steps)
                forecast_list = forecast_result[0]
                '''
                logger.debug("== forecast_list:%s", forecast_list)

                body = {}

                cur_date = prediction_date + timedelta(hours=1)

                for step in range(0, predict_steps):
                    prediction_date = prediction_date + timedelta(hours=1)

                    if cur_date.date() - prediction_date.date() == timedelta(days=-1):
                        ret = client.get_sensor(sensor_id).get_daily_summary_fetcher().append_prediction(sensor_type,
                                                                                                         cur_date.year,
                                                                                                         cur_date.month,
                                                                                                         cur_date.day,
                                                                                                         body)

                        logger.debug("== sensor type: %s, prediction_date: %s, append_prediction: %s, body: %s", sensor_type, cur_date, ret, body)
                        body = {}
                        cur_date = prediction_date
                        body.update({str(prediction_date.hour): {"average": forecast_list[step], "hour": prediction_date.hour}})
                    else:
                        body.update({str(prediction_date.hour): {"average": forecast_list[step], "hour": prediction_date.hour}})

                # update remaining prediction results
                ret = client.get_sensor(sensor_id).get_daily_summary_fetcher().append_prediction(sensor_type,
                                                                                                 cur_date.year,
                                                                                                 cur_date.month,
                                                                                                 cur_date.day,
                                                                                                 body)
                logger.debug("== remaining data, sensor type: %s, prediction_date: %s, append_prediction: %s, body: %s", sensor_type, cur_date, ret, body)

            except BaseException as e:
                return http_error_response("ARIMA Error: " + str(e) + " => sendsor_id: " + sensor_id + ", sensor type: " + sensor_types_dict[sensor_type], 404)

    return http_success_response()

@app.route('/predictions', methods=['GET'])
def predictions():
    warnings.filterwarnings("ignore")

    total_now_time = time.time()

    date_from = request.args.get('from')
    logger.info("== date_from: %s", date_from)

    date_to = request.args.get('to')
    logger.info("== date_to: %s", date_to)


    # Validate date
    if validate_date_string(date_from) == False:
        return http_error_response('Incorrect date format, date_from must be YYYY-MM-DD', 400)

    if validate_date_string(date_to) == False:
        return http_error_response('Incorrect date format, date_to must be YYYY-MM-DD', 400)

    predict_steps = int(request.args.get('step'))
    logger.info("== predict_steps: %s", predict_steps)

    # Validate predict_steps
    if predict_steps <= 0 or predict_steps > 10000:
        return http_error_response('Out of predict_steps range, predict_steps must be [1-10000]', 400)

    predict_unit = request.args.get('unit')
    logger.info("== predict_unit: %s", predict_unit)

    # Validate predict_unit
    if predict_unit != 'hourly' and predict_unit != 'daily':
        return http_error_response('Incorrect predict_unit, predict_unit must be [daily, hourly]', 400)

    split_from = date_from.split('-')
    split_to = date_to.split('-')

    from_date = datetime(int(split_from[0]), int(split_from[1]), int(split_from[2]), 0, 0, 0)
    to_date = datetime(int(split_to[0]), int(split_to[1]), int(split_to[2]), 23, 0, 0)

    if to_date.date() - from_date.date() <= timedelta(days=-1):
        return http_error_response('from date should be earlier than to date', 400)

    # Create AIslandClient object
    now_time = time.time()
    client = AIslandClient.create_client("49.247.210.243", 27017, "apmonv1")
    logger.info("== AIslandClient is created. time elapsed: %f", time.time()-now_time)

    # Get sensors
    now_time = time.time()
    sensors = client.get_sensors()
    logger.info("== sensors list size(%d) is loaded. time elapsed: %f", len(sensors), time.time()-now_time)

    http_response_results = []
    result = http_success_response()
    err_msgs = []

    for sensor in sensors:
            #if sensor.sensor_id == 'sensor02':
            for sensor_type in sensor_types:
                df = pd.DataFrame(columns=['ds', 'y'])

                data_fetcher = DataFetcher.create_daily_summary_fetcher("49.247.210.243", 27017, "apmonv1", sensor.sensor_id, 9)
                current_result = insert_daily_summary_into_mongodb(df, client, data_fetcher, sensor_type, from_date, to_date, predict_unit, predict_steps, sensor.sensor_id)
                http_response_results.append(current_result)

    logger.info("== Total prediction time elapsed: %f", time.time()-total_now_time)

    for msg in http_response_results:
        if msg.status_code != 200:
            json_data = msg.data.decode('utf8')
            json_data = json.loads(json_data)
            err_msgs.append(json_data)

    if len(err_msgs) != 0:
        result = http_error_response(err_msgs, 404)

    return result

def start_tornado(app, port=5000):
    http_server = tornado.httpserver.HTTPServer(
        tornado.wsgi.WSGIContainer(app))
    http_server.listen(port)
    logging.info("Tornado server starting on port {}".format(port))
    tornado.ioloop.IOLoop.instance().start()

def start_from_terminal(app):
    """
        Parse command line options and start the server.
        """
    parser = optparse.OptionParser()
    parser.add_option(
        '-d', '--debug',
        help="enable debug mode",
        action="store_true", default=False)
    parser.add_option(
        '-p', '--port',
        help="which port to serve content on",
        type='int', default=5000)

    opts, args = parser.parse_args()

    if opts.debug:
        app.run(debug=True, host='0.0.0.0', port=opts.port)
    else:
        start_tornado(app, opts.port)

if __name__ == '__main__':
    start_from_terminal(app)
