FROM aislanda/first_step_predictor_base:0.0.1

RUN mkdir /webapp
ADD requirements.txt /webapp
ADD app.py /webapp
ADD entrypoint.sh /webapp

WORKDIR /webapp

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000
ENTRYPOINT ["/bin/bash", "entrypoint.sh"]
